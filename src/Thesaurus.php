<?php

namespace App;

class Thesaurus
{
    /**
     * @var array
     */
    protected $synonyms;

    /**
     * @param array $synonyms
     */
    public function __construct(array $synonyms)
    {
        $this->synonyms = $synonyms;
    }

    /**
     * @param string $word
     * @return string
     */
    public function getSynonyms($word)
    {
        if (!array_key_exists($word, $this->synonyms)) {
            $synonyms = [];
            json_encode(compact('word', 'synonyms'));
        }

        $synonyms = $this->synonyms[$word];
        return json_encode(compact('word', 'synonyms'));
    }
}
