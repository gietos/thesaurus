<?php

class ThesaurusTest extends \PHPUnit_Framework_TestCase
{
    public function dataProvider()
    {
        return [
            [['buy' => ['purchase'], 'big' => ['great', 'large']], 'big', '{"word":"big","synonyms":["great","large"]}'],
        ];
    }

    /**
     * @dataProvider dataProvider
     */
    public function testGetSynonyms($config, $word, $result)
    {
        $thesaurus = new \App\Thesaurus($config);
        return $this->assertEquals($result, $thesaurus->getSynonyms($word));
    }
}
